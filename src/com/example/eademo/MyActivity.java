package com.example.eademo;

import android.app.Activity;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        FrameLayout footerLayout = (FrameLayout) getLayoutInflater().inflate(R.layout.footer,null);
        TextView footer = (TextView) footerLayout.findViewById(R.id.footer);

        ListView lv = (ListView) findViewById(R.id.list_view);
        lv.addFooterView(footerLayout);

        //--page size = 10--
        ProdAdapter ad = new ProdAdapter(this,10, footer);
        lv.setAdapter(ad);

        //--load first 10 items--
        LoaderTask t = new LoaderTask(0,10,this,ad);
        t.execute();

    }
}
